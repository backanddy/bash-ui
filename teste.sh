#!/usr/bin/env bash

. ./bash-ui
. ./config

cls

canvas_fill

draw_title
draw_footer

text_title_right 'Blau Araujo'
text_title_left 'debxp/cursos'

text_footer_right 'Página 0/0'
text_footer_left 'Linha 0/0'
text_footer_center '[Tabela de Conteúdo]'

tput setaf $TEXT_FG
tput setab $TEXT_BG

set_line $CANVAS_START
echo -n "canvas start -> $CANVAS_START"
set_line $CANVAS_END
echo -n "canvas end -> $CANVAS_END"
set_line $CENTER_ROW
echo -n "canvas center -> $CENTER_ROW"
set_pos $CENTER_ROW $CENTER_COL
echo -n '+'

tput sgr0
show_command ':'

